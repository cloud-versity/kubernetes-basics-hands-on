# Basic Kubernetes

## Run locally

`npm install`

`npm start`

http://localhost:3000

## Docker

Docker build: `docker build -t <YOUR_IMAGE_NAME> .`

Docker run: `docker run -p 3000:3000 <YOUR_IMAGE_NAME>`

## Kubernetes

Create deployment: `kubectl create deployment basic-kubernetes --image=cloudversityoffical/basic-kubernetes`

Expose locally via Nodeport: `kubectl expose deployment basic-kubernetes --port=80 --target-port=3000 --type=NodePort`

Get local port: `kubectl get svc`

http://localhost:<SVC_PORT>

This project is inspired by: https://github.com/paulbouwer/hello-kubernetes
