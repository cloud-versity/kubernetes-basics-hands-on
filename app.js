var express = require('express');
var exphbs = require('express-handlebars');
os = require("os");

var app = express();
var PORT = process.env.PORT || 3000

app.engine('handlebars', exphbs());
app.use(express.static('static'));
app.set('view engine', 'handlebars');
app.kee

app.get('/', function (req, res) {
    res.render('home', {
        platform: os.type(),
        release: os.release(),
        hostName: os.hostname(),
    })
    res.set("Connection", "close");
});

app.listen(3000, () => {
    console.log(`Server listening on port: ${os.hostname}:${PORT}`);
});